package info.hccis.assignment.bo;

import java.util.Scanner;

/**
 * Customer class
 *
 * @author bjmaclean
 * @since 20190521
 */
public class Customer {

    private String name;
    private String address1;
    private String address2;
    private String postalCode;
    private String phoneNumber;

    /**
     * Get values for the attributes.
     *
     * @since 20190521
     * @author BJM
     */
    public void getInformation() {
        Scanner input = new Scanner(System.in);

        System.out.println("Customer details");
        System.out.println("\nName (First Last):");
        String tempName = input.nextLine().trim();

        //Validate name
        int positionOfSpace = tempName.indexOf(" ");

        if (positionOfSpace > 0) {
            name = tempName.substring(0, 1).toUpperCase();
            name += tempName.substring(1, positionOfSpace).toLowerCase();
            name += " ";
            name += tempName.substring(positionOfSpace+1, positionOfSpace + 2).toUpperCase();
            name += tempName.substring(positionOfSpace + 2).toLowerCase();
        } else {
            System.out.println("Error: Name is expected to have a space.");
        }

        System.out.println("\nAddress line 1:");
        address1 = input.nextLine();

        System.out.println("\nAddress line 2:");
        address2 = input.nextLine();

        //POSTAL CODE
        System.out.println("\nPostal Code:");
        boolean valid = false;
        //https://howtodoinjava.com/regex/java-regex-validate-canadian-postal-zip-codes/
        String regex = "^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$";
        do {
            postalCode = input.nextLine().toUpperCase();
            if (postalCode.matches(regex)) {
                valid = true;
            } else {
                System.out.println("Please enter a valid Canadian postal code: ");
            }
        } while (!valid);
        
        //PHONE NUMBER
        //https://stackoverflow.com/questions/42104546/java-regular-expressions-to-validate-phone-numbers/42105140
        String pattern = "(?:\\d{3}-){2}\\d{4}";

        System.out.println("\nPhone number (999-999-9999):");
        valid = false;
        do {
            phoneNumber = input.nextLine();
            if (phoneNumber.matches(pattern)) {
                valid = true;
            } else {
                System.out.println("Please enter a valid phone number: ");
            }
        } while (!valid);

        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void display() {
        System.out.println(toString());
    }

    public String toString() {
        String output = "Customer: "+name;
        output += "\n"+address1;
        output += "\n"+address2;
        output += "\n"+postalCode;
        output += "\n"+phoneNumber;
        return output;
    }
}
